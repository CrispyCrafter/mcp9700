from mcp9700 import MCP9700

from machine import ADC
adc = ADC(bits=12)
pin = 'P16'

temp = MCP9700(adc, pin)
while True:
    print(temp.read())