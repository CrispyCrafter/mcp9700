import array

class MCP9700:
    def __init__(self, adc, pin, samples=2048):       
        self.zero = 500 # Millivolt
        self.temp_coeff  = 10.0 # Millivolt/Celsius
        self.samples = samples
        self.channel = adc.channel(pin=pin, attn=adc.ATTN_11DB)

    def get_average(self):            
        buffer = array.array(
            "f", self.channel.value_to_voltage(
                self.channel()
            ) for _ in range(self.samples)
        )
        avg = sum(buffer)/self.samples
        del buffer
        return avg

    def read(self):
        return (self.get_average() - self.zero) / self.temp_coeff

